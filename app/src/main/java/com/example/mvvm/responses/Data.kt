package com.example.mvvm.responses

data class Data(
    val password: List<String>,
    val phone: List<String>
)