package com.example.mvvm.responses

data class LoginResponse(
    val `data`: Data,
    val status: String,
    val statusCode: Int
)